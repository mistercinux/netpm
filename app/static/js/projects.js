    // Functions
    // """""""""

    function displayMessage(message, category='success') {
        $('#message').html('<div class="alert alert-'+category+' alert-dismissible fade show" role="alert"><span>'+message+'</span><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>')
    }

    // Returns the projects list, or the project with the correct ID if the argument is given
    function getProjects(id=0) {
        reqData = {'id': id } ;
        return $.ajax({
            url : '/projects/get',
            type : 'post',
            dataType : 'json',
            contentType: "application/json",
            data: JSON.stringify(reqData),
            success : function(data, status){
                console.log('Get project success')
                console.log(data)
            },
            error : function(data, status, error){
                console.log('Get project Error:')
                console.log(error)
            }
        });
    }

    // Set the desired project as active in database
    function runProject() {
         id=$('#runProjectIdInput').val();
         name=$('#runProjectNameInput').val();
         reqData = { 'projectId': id, 'projectName': name }

        return $.ajax({
            url : '/projects/run',
            type : 'post',
            dataType : 'json',
            contentType: "application/json",
            data: JSON.stringify(reqData),
            success : function(data, status) {
                displayMessage('Project successfully loaded', 'success')
                setTimeout(location.reload.bind(location), 800);            },
            error : function(data, status, error){
                displayMessage('An error occurred while loading the project : '+status+' : '+error, 'danger')
                $('#runProject').modal('hide')
            }
        });


    }

    // Updates the edited project into the database
    function updateProject(){

        id=$('#editProjectIdInput').val();
        name=$('#editProjectNameInput').val();
        description=$('#editProjectDescriptionInput').val();

        reqData = { "projectId": id, "projectName": name, "projectDescription": description }

        return $.ajax({
            url : '/projects/update',
            type : 'post',
            dataType : 'json',
            contentType: "application/json",
            data: JSON.stringify(reqData),
            success : function(data, status) {
                displayMessage('Project information successfully updated', 'success')
                $('#name_'+id).html(name)
                $('#description_'+id).html(description)
                $('#modifyProject').modal('hide')
            },
            error : function(data, status, error){
                displayMessage('An error occurred while updating the project information. Description can be empty, but an project name must be provided.', 'danger')
                $('#modifyProject').modal('hide')
            }
        });

    }


    // Deletes the project with the given ID from database
    function deleteProject(id=0) {
        console.log('test delete project');
        id=$('#deleteProjectIdInput').val();
        reqData = {'id': id } ;
        console.log('id delete = '+id);

        return $.ajax({
            url : '/projects/delete',
            type : 'post',
            dataType : 'json',
            contentType: "application/json",
            data: JSON.stringify(reqData),
            success : function(data, status){
                displayMessage('Project successfully deleted', 'success')
                $('#edit_'+id).parent().parent().remove()
                $('#deleteProject').modal('hide')
            },
            error : function(data, status, error){
                displayMessage('An error occurred while deleting the project', 'danger')
                $('#deleteProject').modal('hide')
            }
        });
    }


// DOCUMENT READY
// """"""""""""""


$(document).ready(function(){

    // Fill and show modals on click
    // """""""""""""""""""""""""""""

    // Edit project button
    $(".edit_icon").click( function (){
        id=$(this).attr('id').split('_')[1];
        project=getProjects(id).done( function(resultat) {
            res=resultat[0]
            $("#editProjectNameInput").val(res.projectName)
            $('#editProjectDescriptionInput').val(res.projectDescription)
            $("#editProjectIdInput").val(res.projectId)
        });
        $('#modifyProject').modal('show');
    })

    // Delete project button
    $(".delete_icon").click( function (){
        id=$(this).attr('id').split('_')[1];
        project=getProjects(id).done( function(resultat) {
            res = resultat[0]
            $("#deleteProjectNameInput").val(res.projectName)
            $('#deleteProjectDescriptionInput').val(res.projectDescription)
            $("#deleteProjectIdInput").val(res.projectId)
        });
        $('#deleteProject').modal('show')
    })

    // Run project button
    $(".run_icon").click( function (){
        id=$(this).attr('id').split('_')[1];
        project=getProjects(id).done( function(resultat) {
        res=resultat[0]
            $("#runProjectNameInput").val(res.projectName)
            $('#RunProjectDescriptionInput').val(res.projectDescription)
            $("#runProjectIdInput").val(res.projectId)
        });
        $('#runProject').modal('show')
    })


    // Others features
    // """""""""""""""

    // Filter table display using the search bar
    $("#tableSearchInput").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#projectsListTable tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });


});