// TODO : EMPÊCHER LA MODIFICATION DE VALEUR SI LE TYPE D'OBJET NE CORRESPOND PAS
    // Il est actuellement possible d'éditer un host, et de changer la valeur en subnet par exemple



function displayMessage(message, category='success') {
    $('#message').html('<div class="alert alert-'+category+' alert-dismissible fade show" role="alert"><span>'+message+'</span><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>')
}

// TODO new func : ValidateIP6address()

function ValidateIP4address(ipaddress) {
  if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ipaddress)) {
    return (true)
  }
  return (false)
}

function ValidateIP4subnet(ipsubnet) {
    networkAddr = ipsubnet.split('/')[0]
    mask = ipsubnet.split('/')[1]
    if (ValidateIP4address(networkAddr)) {
        if ( mask >= 1 && mask <=30 ) {
            return (true)
        }
    }
    return (false)
}

function ValidateIP4range(ip4range) {
    startIP = ip4range.split('/')[0]
    endIP = ip4range.split('/')[1]
    console.log(startIP)
    console.log(endIP)

    if (ValidateIP4address(startIP)){
        if (ValidateIP4address(endIP)){
            for (a=0; a<4; a++) {
                if (parseInt(startIP.split('.')[a]) < parseInt(endIP.split('.')[a])){
                    return (true)
                }
            }
        }
    }
    return (false)
}

// When clicking on OK button in create object modale, check parameters, and add object to database
function addNewNetObject() {
    value = $('#newNetObjectValue').val()
    if ($('#newNetObjectName').val() == '') {
        alert('A name must be provided!')
        return false
    }

    valueType = checkInputObjectValueType($('#newNetObjectValue').val())

    if (! valueType) {
        alert('Please check the goodness of the entered fields...')
        return false
    }

    // TODO check ipv6 format address. (mavar.parseInt(16)

    console.log('test create network object');

    name = $('#newNetObjectName').val()
    description = $('#newNetObjectDescription').val()
    value = $('#newNetObjectValue').val()

    reqData = {'name': name, 'description': description, 'value': value, 'valueType': valueType } ;

    return $.ajax({
        url : '/netObjects/new',
        type : 'post',
        dataType : 'json',
        contentType: "application/json",
        data: JSON.stringify(reqData),
        success : function(data, status){
            displayMessage('Network object successfully created end saved to the database', 'success')
            console.log('ADD NEW object : success')
            $('#newNetObjectModal').modal('hide')
            drawNetObjectsTable();

        },
        error : function(data, status, error){
            displayMessage('An error occurred while add the net object to the database', 'danger')
            console.log('ADD NEW object : FAILED!')
        }
    });
}


function updateNetObject() {

    id = $('#editNetObjectId').val()
    console.log('debug update net object () : id = '+id)

    value = $('#editNetObjectValue').val()
    if ($('#editNetObjectName').val() == '') {
        alert('A name must be provided!')
        return false
    }

    console.log('test UPDATE network object');
    valueType = checkInputObjectValueType($('#editNetObjectValue').val())

    if (! valueType) {
        alert('Please check the goodness of the entered fields... ')
        console.log('detected type : '+valueType)
        return false
    }


    name = $('#editNetObjectName').val()
    description = $('#editNetObjectDescription').val()
    value = $('#editNetObjectValue').val()

    reqData = {'id': id, 'name': name, 'description': description, 'value': value, 'valueType': valueType } ;

    return $.ajax({
        url : '/netObjects/update',
        type : 'post',
        dataType : 'json',
        contentType: "application/json",
        data: JSON.stringify(reqData),
        success : function(data, status){
            displayMessage('Network object successfully updated', 'success')
            console.log('UPDATE object : success')
            $('#editNetObjectModal').modal('hide')
            drawNetObjectsTable();

        },
        error : function(data, status, error){
            displayMessage('An error occurred while UPDATE the net object to the database', 'danger')
            console.log('UPDATE object : FAILED!')
        }
    });

}

function deleteNetObject() {

    id = $('#deleteNetObjectId').val()
    console.log('debug delete net object () : id = '+id)

    value = $('#deleteNetObjectValue').val()

    console.log('test DELETE network object');
    reqData = {'id': id} ;

    return $.ajax({
        url : '/netObjects/delete',
        type : 'post',
        dataType : 'json',
        contentType: "application/json",
        data: JSON.stringify(reqData),
        success : function(data, status){
            displayMessage('Network object successfully deleted', 'success')
            console.log('UPDATE object : success')
            $('#deleteNetObjectModal').modal('hide')
            drawNetObjectsTable();

        },
        error : function(data, status, error){
            displayMessage('An error occurred while DELETING the net object to the database', 'danger')
            console.log('DELETE object : FAILED!')
        }
    });
}


function checkInputObjectValueType(value) {

        if (ValidateIP4range(value)) {
            $('.validateValueMessage').html('<div class="alert alert-success">Valid ip range</div>')
            valueType = 'range'
        }
        else if (ValidateIP4address(value)) {
            $('.validateValueMessage').html('<div class="alert alert-success">Valid ipv4 host</div>')
            valueType = 'host'
        }
        else if (ValidateIP4subnet(value)) {
            $('.validateValueMessage').html('<div class="alert alert-success">Valid ipv4 subnet</div>')
            valueType = 'subnet'
        }
        else {
            $('.validateValueMessage').html('<div class="alert alert-danger">Incorrect syntax</div>')
            return false
        }

        if (value == $('#newNetObjectValue').val())
            selectType = $('#newNetObjectType').val()
        if (value == $('#editNetObjectValue').val())
            selectType = $('#editNetObjectType').val()
        if (value == $('#deleteNetObjectValue').val())
            selectType = $('#deleteNetObjectType').val()


        if (valueType != selectType) {
            $('.validateValueMessage').html('<div class="alert alert-danger">Mismatch in object type syntaxe</div>')
            return false
        }
        return valueType
}

function drawNetObjectsTable() {
    return $.ajax({
        url : '/netObjects/drawTable',
        type : 'post',
        dataType : 'json',
        contentType: "application/json",
        success : function(data, status){
            console.log("draw succeeded")
            $('#objectsTableDiv').html(data)

        },
        error : function(data, status, error){
            displayMessage('An error occurred while getting the net objects table', 'danger')
            console.log('draw table: FAILED!')
        }
    });
}

    // Returns the object informations
    function getObject(id=-1) {
        reqData = {'id': id } ;
        return $.ajax({
            url : '/netObjects/get',
            type : 'post',
            dataType : 'json',
            contentType: "application/json",
            data: JSON.stringify(reqData),
            success : function(data, status){
                console.log('Get net object success')
            },
            error : function(data, status, error){
                console.log('Get net object Error:')
                console.log(error)
            }
        });
    }


$(document).ready(function(){

    drawNetObjectsTable();

    $('#newNetObjectValue').keyup( function() {
        checkInputObjectValueType($('#newNetObjectValue').val())
    });

    $('#newNetObjectType').change( function() {
        checkInputObjectValueType($('#newNetObjectValue').val())
    });

    $('#editNetObjectValue').keyup( function() {
        checkInputObjectValueType($('#editNetObjectValue').val())
    });

    $('#deleteNetObjectValue').keyup( function() {
        checkInputObjectValueType($('#deleteNetObjectValue').val())
    });


    // Edit object button
    $("#objectsTableDiv").on("click", ".edit_icon", function (){
        id=$(this).attr('id').split('_')[1];
        console.log('object ID = '+id)
        getObject(id)
        project=getObject(id).done( function(resultat) {
            console.log('inside............')
            console.log(resultat)
            data=resultat
            $('#editNetObjectName').val(data.name)
            $('#editNetObjectDescription').val(data.description)
            $('#editNetObjectValue').val(data.value)
            $('#editNetObjectType').val(data.type)
            $('#editNetObjectId').val(data.id)
        })
        $('.validateValueMessage').html('')
        $('#editNetObjectModal').modal('show');
    });

    // Delete object button
    // TODO : NOT WORKING...
    $("#objectsTableDiv").on("click", ".delete_icon", function (){
        console.log('Clicked on delete button')
        id=$(this).attr('id').split('_')[1];
        console.log('object ID = '+id)
        getObject(id)
        project=getObject(id).done( function(resultat) {
            console.log(resultat)
            data=resultat
            $('#deleteNetObjectName').val(data.name)
            $('#deleteNetObjectDescription').val(data.description)
            $('#deleteNetObjectValue').val(data.value)
            $('#deleteNetObjectType').val(data.type)
            $('#deleteNetObjectId').val(data.id)
        })
        $('.validateValueMessage').html('')
        $('#deleteNetObjectModal').modal('show')
    })

});
