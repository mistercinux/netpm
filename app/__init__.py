from Classes.DbManager import DbManager
from Classes.Matrix import Matrix
from flask import Flask

# Application creation function is required by Flask
# This allows the config objects to be shared through all the application
def create_app(matrix):
    app = Flask(__name__)
    app.config['matrix'] = matrix
    app.secret_key = 'nRSAUIERsrtateniecTSRUIER'

    return app


# Create the requiered shared objects, and create the app
matrix = Matrix()
app = create_app(matrix)

from app import views





