import json
import filecmp
import sys
from app import app
from Classes.DbManager import DbManager
from Classes.NetObjects import NetParentObject, NetworkHost, NetworkRange, NetworkSubnet
from flask import render_template, request, redirect, session, jsonify, make_response, flash, url_for

# app.config['matrix'] is a Matrix object


@app.route('/')
def main():

    if "projectId" not in session:
        flash("You do not have loaded a project yet. Create a new one, or run an existing project before accessing the policy matrix table", "warning")
        return redirect(url_for('projects'))

    app.config['matrix'].dump()

    return render_template('public/matrix.html', header_title="Network Policy Matrix")


@app.route('/about')
def about():
    return render_template("public/about.html", header_title="About NetPM")

@app.route('/save')
def save():
    dbm = DbManager()
    if dbm.save():
        flash('Project was successfully saved.', 'success')
    else:
        flash('An errror occured while saving the project', 'danger')
    return redirect(url_for('main'))

@app.route("/discardChanges")
def discardChanges():
    dbm = DbManager()
    if dbm.discardChanges():
        flash('Changes were successfully discarded', 'success')
    else:
        flash('An orror uccured while discarding the changes.','danger')
    return redirect(url_for('main'))

@app.route('/shutdown')
def shutdown():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()
    return "Good bye!"


 ##################
 # Projects views #
 ##################


@app.route('/projects', methods=['GET', 'POST'])
def projects():

    dbm = DbManager()
    projectsList = dbm.getProjects()
    for k, v in session.items():
        print('session K:'+k+', = '+str(v))

    if 'projectId' in session:
        print('Running projectId = '+str(session['projectId']))
    return render_template('public/projects.html', projectsList=projectsList)

@app.route('/projects/get', methods=['POST'])
def getProjects():

    # Check if an ID was passed as argument
    req = request.get_json()

    if request.is_json and request.get_json()['id']:
        id = request.get_json()['id']

    dbm = DbManager()
    if "id" in locals():
        projectList = dbm.getProjects(id)
    else:
        projectList = dbm.getProjects()

    if projectList:
        res = make_response(jsonify(projectList), 200)
    else:
        res = make_response("No projects in found", 400)

    return res

@app.route('/projects/addNew', methods=["GET","POST"])
def newProject():

    errorMessage = None
    successMessage = None
    dbm = DbManager()

    if request.method == 'POST':
        req = request.form
        if req['projectName'] != '':
            print('Register the project, and create the matrix...')
            if dbm.createNewProject(req['projectName'], req['projectDescription']) and dbm.save():
                flash("New project sucessfully created and saved to the database!", 'success')
            else:
                flash("Error while creating the new project : "+dbm.errorMessage, 'danger')
        else:
            flash("Error while creating the new project: You must enter a project Name", 'danger')

    return redirect(url_for('projects'))


@app.route('/projects/update', methods=['POST'])
def updateProject():

    if request.method == "POST":
        # Check if an ID was passed as argument
        if request.is_json and request.get_json()['projectId'] and request.get_json()['projectName']:
            id = request.get_json()['projectId']
            name = request.get_json()['projectName']

            if request.get_json()['projectId']:
                description = request.get_json()['projectDescription']
            else:
                description = ''

            print('ID = ' + id + ' - Name = ' + name + ' - Description = ' + description)
            dbm = DbManager()
            if dbm.updateProject(id, name, description):
                return make_response(jsonify('success'), 200)

    return make_response(jsonify('An error occured while updating the database. Please check if all arguments are passed to the function'), 400)


@app.route('/projects/run', methods=['POST'])
def runProject():
    if request.method == "POST":

        print(session)
        dbm = None

        if request.is_json:
            req = request.get_json()
            if "projectId" in req:
                projectId = req['projectId']
                if "projectName" in req:
                    projectName = req['projectName']
                    dbm = DbManager()

        if not dbm:
            print('Poject ID and project NAME are required')
            return make_response(jsonify('NOK'), 400)

            # TODO check for unsaved changes before load new project.

        if( dbm.setActiveProject(projectId)):
            print('Loading project...')
            session['projectId'] = int(projectId)
            session['projectName'] = projectName
            return make_response(jsonify('OK'), 200)

    print('debug : project not loaded...')
    return make_response(jsonify('NOK'), 400)

@app.route('/projects/delete', methods=['POST'])
def deleteProject():

    # Check if an ID was passed as argument
    if request.is_json and request.get_json()['id']:
        id = request.get_json()['id']
        req = request.get_json()
    else:
        return make_response(jsonify('No valid ID passed as argument'), 400)

    if id:
        dbm = DbManager()    # WARNING : PLEASE DELETE THE NET OBJECTS ASSOCIATED WITH THE PROJECT AS WELL.
        dbm.deleteProject(id)
        return make_response(jsonify('success'), 200)

    return make_response(jsonify('error'), 400)


 ####################
 # NetObjects views #
 ####################

@app.route('/netObjects')
def netObjects():
    return render_template('public/netObjects.html')


@app.route('/netObjects/drawTable', methods=['GET', 'POST'])
def drawNetObjectsTable():
    if not 'projectId' in session:
        return 'No project loaded...'

    projectId = session['projectId']
    dbm = DbManager()
    netobjects = dbm.getNetworkObjectsByProjectId(session['projectId'])

    return jsonify(render_template('public/tables/netobjects_table.html', lst_netobjects=netobjects))


@app.route('/netObjects/get', methods=['POST'])
def getNetObject():
    if request.is_json:
        req = request.get_json()
        if 'id' in req and int(req['id']) > 0:
            dbm = DbManager()
            netobject = dbm.getNetObject(req['id'])
            return make_response(jsonify(netobject), 200)
        else:
            print('No valid ID passed to the getNetObject view')
            return make_response(jsonify('NOK', 400))

@app.route('/netObjects/new', methods=['POST'])
def addNetObject():
    if request.is_json:
        req = request.get_json()
        if req['name'] != '' and req['value'] != '':
            name = str(req['name'])
            description = str(req['description'])
            value = str(req['value'])
            valueType = str(req['valueType'])

            dbm = DbManager()

            if valueType == 'host':
                netobject = NetworkHost(name, description, value, session['projectId'])
            elif valueType == 'subnet':
                netobject = NetworkSubnet(name, description, value, session['projectId'])
            elif valueType == 'range':
                netobject = NetworkRange(name, description, value, session['projectId'])
            else:
                print('ERROR: Unknown type of value.')
                return make_response(jsonify('Value type error'), 400)

            try:
                if dbm.addNetworkObject(netobject):
                    print('object added successfully')
                else:
                    print('error while adding to database')
                    print(dbm.errorMessage)
                return make_response(jsonify('OK'), 200)
            except :
                print('An error occured while trying to add the object to the database.'+str(sys.exc_info()[0]))
                return make_response(jsonify('Failed to add object to database'), 400)

    else:
        print('NO POST...')

@app.route('/netObjects/update', methods=['POST'])
def updateNetObject():
    if request.is_json:
        req = request.get_json()
        if req['name'] != '' and req['value'] != '' and req['id'] != '':
            name = str(req['name'])
            ID = str(req['id'])
            description = str(req['description'])
            value = str(req['value'])
            valueType = str(req['valueType'])

            dbm = DbManager()

            if valueType == 'host':
                object = NetworkHost(name, description, value, session['projectId'], int(ID))
            elif valueType == 'subnet':
                object = NetworkSubnet(name, description, value, session['projectId'], int(ID))
            elif valueType == 'range':
                object = NetworkRange(name, description, value, session['projectId'], int(ID))
            else:
                return make_response(jsonify('NO VALID TYPE'), 400)

            if (dbm.updateNetObject(object)):
                return make_response(jsonify('UPDATE NET OBJECT : SUCCESS'), 200)
            else:
                return make_response(jsonify('An error occurred while updating the database'), 400)

@app.route('/netObjects/delete', methods=['POST'])
def deleteNetObjects():
    if request.is_json:
        req = request.get_json()
        print('debug deleteNetObject()')
        if req['id'] != '':
            print('VIEW: netObjects/delete : ID = '+str(req['id']))

        dbm = DbManager()
        ID = req['id']
        if (dbm.deleteNetObject(int(ID))):
            return make_response(jsonify('SUCCESS'), 200)
        else:
            return make_response(jsonify('Error while deleting object ID '+str(id)), 400)
    return make_response(jsonify('JSON format required'), 400)



