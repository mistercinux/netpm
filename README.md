# NetPM project



## What is NetPM, or... what will it be ?
Network Policie Matrix allows you to quickly design your network policy flow matrix.

At step 1, you will create network objects that are hosts, subnets, or ip ranges, and assign 
them to the source and/or destination into the matrix.

Next step is to place the different services, and applications into the matrix.

Finally, NetPM will give you a nice overview of the policies that should be enabled on your
security equipments.
It will also be possible to filter the view from source / destination or both of them.


## Some debug tools
- json code check: https://jsoneditoronline.org/#left=local.xuduyo&right=local.sagajo
- sqlite cli : https://www.sqlite.org/cli.html
- sqlitebrowser : usefull sqlite database browser


## Python dev notes :

  variables for table names : t_table_name
  variables for views :       v_view_name
  

## The différents objects in NetPM

###SNA (Services and Applications)
Objects called SNA can be:
- Service : couple protocol / port
- Application : application web ou autre (ex: facebook, teamviewer, etc.)

###Network objects
Network objects can be :
- Host
- Subnet
- IP Range

## DATABASE structure

The field is_custom in the tables is set to 1 by default. 
Values passed in the fresh installed bdd are set to 0. 
This should (in futur) allow an export from the custom objects without having to import the full list again.

- Applications and services are objects that will be placed inside the matrix 
- Network objects are network hosts, subnets, or ip ranges, identity objects and will be used in the source / destination part of the matrix

### Projects
~~~~sql
CREATE TABLE "projects" (
	"id"	integer PRIMARY KEY AUTOINCREMENT,
	"name"	text uniq not null,
	"description" text,
	"json_project_data"	text not null,
	"is_active" boolean default 0
);
~~~~

###Applications
~~~~sql
create table applications (
  id integer primary key autoincrement,
  name text uniq,
  description text,
  is_custom boolean default 1
);
~~~~


###Services
~~~~sql
create table services (
  id integer primary key autoincrement,
  name text uniq,
  description text,
  protocol integer,  
  port integer,
  is_custom boolean default 1
);
~~~~


(tcp=6 , udp=17,...) https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml

###Applications groups
~~~~sql
create table applications_groups (
  id integer primary key autoincrement,
  name text uniq,
  description text,
  is_custom boolean default 1
);
~~~~

###Services groups
~~~~sql
create table services_groups (
  id integer primary key autoincrement,
  name text uniq,
  description text,
  is_custom boolean default 1
);
~~~~


###Applications groups association table
~~~~sql
create table application_groups_assoc_table (
  id integer primary key autoincrement,
  id_group integer not null,
  id_objet integer not null,
  is_custom boolean default 1,
  FOREIGN KEY (id_group)
    REFERENCES applications_groups (id)
      ON UPDATE CASCADE
      ON DELETE CASCADE,
  FOREIGN KEY (id_objet)
    REFERENCES applications (id)
      ON UPDATE CASCADE
      ON DELETE CASCADE
);
~~~~

###Services groups association table
~~~~sql
create table services_groups_assoc_table (
  id integer primary key autoincrement,
  id_group integer not null,
  id_objet integer not null,
  is_custom boolean default 1,
  FOREIGN KEY (id_group)
    REFERENCES services_groups (id)
      ON UPDATE CASCADE
      ON DELETE CASCADE,
  FOREIGN KEY (id_objet)
    REFERENCES services (id)
      ON UPDATE CASCADE
      ON DELETE CASCADE
);
~~~~
### Simple view of applications and groups
~~~~sql

create view v_applications_by_groups as 
  select 
      applications_groups.id as id_group,
      applications_groups.name as group_name,
      applications_groups.description as group_description,
      applications.id as applictaion_id,
      applications.name as application_name,
      applications.description as application_description
    from application_groups_assoc_table 
      inner join applications_groups on application_groups_assoc_table.id_group = applications_groups.id 
      inner join applications on applications.id = application_groups_assoc_table.id_objet;
~~~~



Basic select sample with this view : select * from v_applications_groups where id_group = 1;


### Simple view of applications and groups
~~~~sql

create view v_services_by_groups as 
  select 
      services_groups.id as id_group,
      services_groups.name as group_name,
      services_groups.description as group_description,
      services.id as applictaion_id,
      services.name as application_name,
      services.description as application_description
    from services_groups_assoc_table
      inner join services_groups on services_groups_assoc_table.id_group = services_groups.id 
      inner join services on services.id = services_groups_assoc_table.id_objet;
~~~~

Basic select sample with this view : select * from v_services_groups where id_group = 1;


### Network objects
~~~~sql
create table network_objects (
  id integer primary key autoincrement,
  name text not null,
  description text,
  ip_version integer,
  is_custom boolean default 1,
  type int not null,
  value text not null,
  projectId not null
);
~~~~

Les valeurs possibles pour certains champs sont les suivantes :
- ip_version
    - ipv4 = 4
    - ipv6 = 6
  
- type 
    - host    = 1
    - subnet  = 2
    - range   = 3
  
- value
    - host : 192.168.1.1
    - subnet : 10.1.0.0/22
    - range : 172.16.100.101/172.16.101.200
  
### Network groups
~~~~sql
create table network_groups (
  id integer primary key autoincrement,
  name text uniq,
  description text,
  is_custom boolean default 1
);
~~~~

### Network groups association table
~~~~sql
create table network_groups_assoc_table (
  id integer primary key autoincrement,
  id_group integer not null,
  id_objet integer not null,
  is_custom boolean default 1,
  FOREIGN KEY (id_group)
    REFERENCES network_groups (id)
      ON UPDATE CASCADE
      ON DELETE CASCADE,
  FOREIGN KEY (id_objet)
    REFERENCES network_objects (id)
      ON UPDATE CASCADE
      ON DELETE CASCADE
);
~~~~

### Simple view of network objects and groups
~~~~sql

create view v_network_objects_by_groups as 
  select 
      network_groups.id as id_group,
      network_groups.name as group_name,
      network_groups.description as group_description,
      network_objects.id as network_object_id,
      network_objects.name as network_object_name,
      network_objects.description as network_object_description
    from network_groups_assoc_table
      inner join network_groups on network_groups_assoc_table.id_group = network_groups.id 
      inner join network_objects on network_objects.id = network_groups_assoc_table.id_objet;
~~~~

### Identity objects 
Identity objects will identify users in an active directory or other user identification mechanism
~~~~sql
create table identities (
  id integer primary key autoincrement,
  name text uniq,
  description text,
  is_custom boolean default 1
);
~~~~

### Identity groups
Identity groups contains multiple users. Like AD groups.
~~~~sql
create table identities_groups (
  id integer primary key autoincrement,
  name text uniq,
  description text,
  is_custom boolean default 1
);
~~~~

### Identities groups association table
~~~~sql
create table identities_groups_assoc_table (
  id integer primary key autoincrement,
  id_group integer not null,
  id_objet integer not null,
  is_custom boolean default 1,
  FOREIGN KEY (id_group)
    REFERENCES identities_groups (id)
      ON UPDATE CASCADE
      ON DELETE CASCADE,
  FOREIGN KEY (id_objet)
    REFERENCES identities (id)
      ON UPDATE CASCADE
      ON DELETE CASCADE
);
~~~~

### Simple view of identities and groups
~~~~sql

create view v_identities_by_groups as 
  select 
      identities_groups.id as id_group,
      identities_groups.name as group_name,
      identities_groups.description as group_description,
      identities.id as identity_id,
      identities.name as identity_name,
      identities.description as identity_description
    from identities_groups_assoc_table
      inner join identities_groups on identities_groups_assoc_table.id_group = identities_groups.id 
      inner join identities on identities.id = identities_groups_assoc_table.id_objet;
~~~~


## Ideas 
It should be possible to display things like this :
  - The full scrollable matrix (top/down/left/right scroll) with zoom.
  - Selecting the source object(s), it could be usefull to see différent displays with only those sources and or destinations
  - Filtering matrix with 'and', 'or', 'src', 'dst', ...
  - Display a simplified view from filtered by sources and/or destination
  - Allow rule optimization for redundant policies ()
  - The services/application icons will be drag and droped from menu or matrix to one matrix table box
