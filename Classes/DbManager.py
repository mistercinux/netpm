import filecmp
import sys

from Classes.NetObjects import *
from Classes.SimpleObjects import *
from Classes.Matrix import *
import sqlite3
from shutil import copyfile
from sys import exit

# SQL WORKING : CODER LES GETTERS ET SETTERS DE BDD

class DbManager:

    """
    This class defines de sqlite database management
    """

    def __init__(self, filepath:str='/home/cinux/.config/NetPM/NetPM.db'):

        self.dbFilePath   = str(filepath)
        self.dbTmpFilePath = str(filepath)+'.tmp'
        self.cursor     = None
        self.con        = None # Connection to the temporary database
        self.conOriginalFile    = None # Connection to the original file database
        self.errorMessage = ''

        # DB tables names
        self.t_applications = 'applications'
        self.t_applications_groups = 'applications_groups'
        self.t_applications_groups_assoc_table = 'applications_groups_assoc_table'
        self.t_services = 'services'
        self.t_services_groups = 'services_groups'
        self.t_services_groups_assoc_table = 'services_groups_assoc_table'
        self.t_network_objects = 'network_objects'
        self.t_network_groups = 'network_groups'
        self.t_network_groups_assoc_table = 'network_groups_assoc_table'
        self.t_identities = 'identities'
        self.t_identities_groups = 'identities_groups'
        self.t_identities_groups_assoc_table = 'identities_groups_assoc_table'
        self.t_projects = 'projects'

        # DB vue names
        self.v_applicationsByGroups = 'v_applications_by_groups'
        self.v_servicesByGroups = 'v_services_by_groups'
        self.v_networkObjectsByGroups = 'v_network_objects_by_groups'
        self.v_identitiesByGroups = 'v_identities_by_groups'

        # Create a temp working db file if it not already exists
        # and connect to this temp file
        try:
            f = open(self.dbTmpFilePath)
            f.close()
        except IOError:
            print("File not accessible or do not exists. Trying to create it...")
            # Do something with the file
            try:
                copyfile(self.dbFilePath, self.dbTmpFilePath)
            except IOError as e:
                print("Unable to copy file. %s" % e)
                exit(1)
            except:
                print("Unexpected error:", sys.exc_info())
                exit(1)

        # Connect to the tmp db file
        self.connect(self.dbTmpFilePath)

    def __del__(self):
        if self.conOriginalFile:
            self.conOriginalFile.close()
        if self.con:
            self.con.close()

    def connect(self, filepath: str = ''):
        """
        Connection to the objects database.
        :param filepath:
        :return:
        """

        try:
            self.con = sqlite3.connect(filepath)
        except sqlite3.DatabaseError as err:
            print("An error occurred when trying to connect to the database. File target : "+self.dbFilePath)
            print(str(err))
            exit(1)
        except sqlite3.Error as err:
            print("An unknown error occured while connecting to che database."+str(err))
            exit(1)
        self.cursor = self.con.cursor()


    def save(self):
        """
        This function saves the tmp working database, to the original one.
        :return:
        """
        # Connect to the original/real database
        try:
            self.conOriginalFile = sqlite3.connect(self.dbFilePath)
        except sqlite3.DatabaseError as err:
            print("An error occurred when trying to connect to the database. File target : "+self.dbFilePath)
            print(str(err))
            exit(1)
        except sqlite3.Error as err:
            print("An unknown error occured while connecting to che database."+str(err))
            exit(1)

        # Try to save the project to the original/real DB
        try:
            print('saving to file...')
            self.con.backup(self.conOriginalFile)
        except sqlite3.DatabaseError as err:
            print("An error occurred when trying to connect to the database. File target : "+self.dbFilePath)
            print(str(err))
            return False
        except sqlite3.Error as err:
            print("An unknown error occurred while connecting to che database."+str(err))
            return False

        return True

    def discardChanges(self):
        """
        This function discards changes the tmp working database, and copy the working to the original one.
        :return:
        """
        # Connect to the original/real database
        try:
            self.conOriginalFile = sqlite3.connect(self.dbFilePath)
        except sqlite3.DatabaseError as err:
            print(str(err))
            exit(1)
        except sqlite3.Error as err:
            print("An unknown error occured while connecting to che database." + str(err))
            exit(1)

        # Try to discards the project changes on the temp database
        try:
            self.conOriginalFile.backup(self.con)
        except sqlite3.DatabaseError as err:
            print(str(err))
            return False
        except sqlite3.Error as err:
            print("An unknown error occurred while discarding changes: " + str(err))
            return False

        return True

    ###########
    # GETTERS #
    ###########

    def getProjects(self, id=None):
        """
        Returns a list of tuples with all projects.
        If an ID is provided, the returned value is filtered with the id
        :param id:
        :return:
        """
        table = self.t_projects

        if id:
            self.cursor.execute("select * from {table} {where} ORDER BY name".format(table=table, where='where id=?'), (int(id),))
        else:
            self.cursor.execute("select * from {table} ORDER BY name".format(table=table))
        resData = self.cursor.fetchall()

        # Create an array with the values
        ret = []
        for a in resData:
            ret.append({"projectId": a[0], "projectName": a[1], "projectDescription": a[2], "projectData": a[3]})

        return ret

    def deleteProject(self, id:int):
        """
        Deletes the project in projects table where id=id,
        and also deletes the net objects where projectId=id
        :param id:
        :return:
        """

        try:
            # With statement, auto commits the commands
            with self.con as con:
                print('Start deleting objects')
                con.execute("delete from {table} where id=?".format(table=self.t_projects), (int(id),))
                con.execute("delete from {table} where projectId=?".format(table=self.t_network_objects), (int(id),))
                con.execute("select * from {table} where projectId=?".format(table=self.t_network_objects), (int(id),))
        except sqlite3.Error as err:
            print("An error occurred while trying to delete the database objects: "+str(err))
            return False

        return True


    def getApplications(self, ID = None):
        """
        Returns a list of tuples with all Applications.
        If an ID is provided, the returned value is filtered with the id
        :param id:
        :return:
        """

        table = self.t_applications
        if ID:
            self.cursor.execute("select * from {table} {where}".format(table=table, where='where id=?'), (ID,))
        else:
            self.cursor.execute("select * from {table}".format(table=table))
        return self.cursor.fetchall()

    def getApplicationsGroups(self, idGroup=None):
        """
        Returns a list of tuples with all application groups.
        If an ID is provided, the returned value is filtered with the id
        :param idGroup:
        :return:
        """

        table = self.t_applications_groups
        if idGroup:
            self.cursor.execute("select * from {table} {where}".format(table=table, where='where id=?'), (idGroup,))
        else:
            self.cursor.execute("select * from {table}".format(table=table))
        return self.cursor.fetchall()


    def getApplicationsByGroups(self, idGroup=None):
        """
        Returns a list of tuples with all application and associeted groups.
        If an ID is provided, the returned value is filtered with the id
        :param idGroup:
        :return:
        """

        table = self.v_applicationsByGroups
        if idGroup:
            self.cursor.execute("select * from {table} {where}".format(table=table, where='where id_group=?'), (idGroup,))
        else:
            self.cursor.execute("select * from {table}".format(table=table))
        return self.cursor.fetchall()

    def getServices(self, id=None):
        """
        Returns a list of tuples with all services.
        If an ID is provided, the returned value is filtered with the id
        :param id:
        :return:
        """

        table = self.t_services
        if id:
            self.cursor.execute("select * from {table} {where}".format(table=table, where='where id=?'), (id,))
        else:
            self.cursor.execute("select * from {table}".format(table=table))
        return self.cursor.fetchall()

    def getServicesGroups(self, idGroup=None):
        """
        Returns a list of tuples with all services groups.
        If an ID is provided, the returned value is filtered with the id
        :param idGroup:
        :return:
        """

        table = self.t_services_groups
        if idGroup:
            self.cursor.execute("select * from {table} {where}".format(table=table, where='where id=?'), (idGroup,))
        else:
            self.cursor.execute("select * from {table}".format(table=table))
        return self.cursor.fetchall()


    def getServicesByGroups(self, idGroup=None):
        """
        Returns a list of tuples with all services and associated groups.
        If an ID is provided, the returned value is filtered with the id
        :param idGroup:
        :return:
        """

        table = self.v_servicesByGroups
        if idGroup:
            self.cursor.execute("select * from {table} {where}".format(table=table, where='where id_group=?'), (idGroup,))
        else:
            self.cursor.execute("select * from {table}".format(table=table))
        return self.cursor.fetchall()

    def getNetObject(self, id):

        if not int(id) > 0:
            return False

        table = self.t_network_objects
        self.cursor.execute("select * from {table} where id=?".format(table=table),(int(id),))
        res = self.cursor.fetchall()[0]
        objType = {1: 'host', 2: 'subnet', 3: 'range'}

        if res:
            ret = {}
            ret['id'] = res[0]
            ret['name'] = res[1]
            ret['description'] = res[2]
            ret['ip_version'] = res[3]
            ret['is_custom'] = res[4]
            ret['type'] = objType.get(res[5], 'unknown type')
            ret['value'] = res[6]
            ret['projectId'] = res[7]
            print('debug getNetObj class')
            print(res)
            return ret
        return False


    def getHosts(self):
        """
        Returns a list of tuples with all network objets with the type 1 (IP)
        :return:
        """

        table = self.t_network_objects
        self.cursor.execute("select * from {table} where type=1".format(table=table))
        return self.cursor.fetchall()

    def getSubnets(self):
        """
        Returns a list of tuples with all network objets with the type 2 (Subnets)
        :return:
        """

        table = self.t_network_objects
        self.cursor.execute("select * from {table} where type=2".format(table=table))
        return self.cursor.fetchall()

    def getIpRanges(self):
        """
        Returns a list of tuples with all network objets with the type 3 (IP range)
        :return:
        """

        table = self.t_network_objects
        self.cursor.execute("select * from {table} where type=3".format(table=table))
        return self.cursor.fetchall()



    def loadNetworkNode(self, id:int):
        """
        Loads and returns a network object from the database, or False if something went wrong
        :param id:
        :return:
        """

        try:
            table = self.t_network_objects
            self.cursor.execute("select * from {table} where id = {id}".format(table=table, id=int(id)))
            data = self.cursor.fetchall()[0]
            name = data[1]
            description = data[2]
            value = data[-1]
            objectType = data[5]
            dbId = data[0]

            print(data)
            if int(objectType) == 1:
                return NetworkHost(name, description, value, dbId)
            elif int(objectType) == 2:
                return NetworkSubnet(name, description, value, dbId)
            elif int(objectType) == 3:
                return NetworkRange(name, description, value, dbId)
            else:
                return False

        except:
            self.errorMessage = "Error occurred while trying to fetch network object from database."
            print(self.errorMessage)
            return False

    def loadService(self, id:int):
        """
        This function loads a service from database, and returns a service object, or False
        :param id: database ID field
        :return:
        """

        try:
            table = self.t_services
            self.cursor.execute("select * from {table} where id = {id}".format(table=table, id=int(id)))
            data = self.cursor.fetchall()[0]

            dbId = int(data[0])
            name = str(data[1])
            description = str(data[2])
            protocol = int(data[3])
            port = int(data[4])
            custom = int(data[5])

            return Service(name, description, protocol, port, dbId, custom)

        except:
            print('debug : error DbManager.loadService()')
            return False

    def loadApplication(self, id:int):
        """
        This function loads a service from database, and returns a service object, or False
        :param id: database ID field
        :return:
        """

        try:
            table = self.t_applications
            self.cursor.execute("select * from {table} where id = {id}".format(table=table, id=int(id)))
            data = self.cursor.fetchall()[0]

            dbId = int(data[0])
            name = str(data[1])
            description = str(data[2])
            custom = int(data[3])

            return Application(name, description, dbId, custom)

        except:
            print('debug : error DbManager.loadService()')
            return False


    def getNetworkGroups(self, idGroup=None):
        """
        Returns a list of tuples with all network groups.
        If an ID is provided, the returned value is filtered with the group id
        :param idGroup:
        :return:
        """

        table = self.t_network_groups
        if idGroup:
            self.cursor.execute("select * from {table} {where}".format(table=table, where='where id=?'), (idGroup,))
        else:
            self.cursor.execute("select * from {table}".format(table=table))
        return self.cursor.fetchall()

    def getNetworkObjectsByGroups(self, idGroup=None):
        """
        Returns a list of tuples with all network nodes and associated groups.
        If an ID is provided, the returned value is filtered with the group id
        :param idGroup:
        :return:
        """

        table = self.v_networkObjectsByGroups
        if idGroup:
            self.cursor.execute("select * from {table} {where}".format(table=table, where='where id_group=?'), (idGroup,))
        else:
            self.cursor.execute("select * from {table}".format(table=table))
        return self.cursor.fetchall()

    def getNetworkObjectsByProjectId(self, projectId:int):
        table = self.t_network_objects

        self.cursor.execute("select * from {table} {where} ORDER BY name,value".format(table=table, where='where projectId=?'), (projectId,))
        objects = self.cursor.fetchall()
        lst_NetObjects = []
        for a in objects:
            if a[5] == 1:
                objType = 'host'
            elif a[5] == 2:
                objType = 'subnet'
            else:
                objType = 'range'

            lst_NetObjects.append( {'id': a[0],
                                    'name': a[1],
                                    'description': a[2],
                                    'ipversion': a[3],
                                    'isCustom': a[4],
                                    'type': objType,
                                    'value': a[6],
                                    'projectId': a[7]
                                    })
        print(lst_NetObjects)
        return lst_NetObjects

    def getIdentities(self, id=None):
        """
        Returns a list of tuples with all identities.
        If an ID is provided, the returned value is filtered with the id
        :param id:
        :return:
        """

        table = self.t_identities
        if id:
            print('debug 1')
            self.cursor.execute("select * from {table} {where}".format(table=table, where='where id=?'), (id,))
        else:
            print('debug 2')
            self.cursor.execute("select * from {table}".format(table=table))
        return self.cursor.fetchall()

    def getIdentitiesGroups(self, idGroup=None):
        """
        Returns a list of tuples with all identity groups.
        If an ID is provided, the returned value is filtered with the group id
        :param idGroup:
        :return:
        """

        table = self.t_identities_groups
        if idGroup:
            self.cursor.execute("select * from {table} {where}".format(table=table, where='where id=?'), (idGroup,))
        else:
            self.cursor.execute("select * from {table}".format(table=table))
        return self.cursor.fetchall()


    def getIdentitiesByGroups(self, idGroup=None):
        """
        Returns a list of tuples with all identities and associated groups.
        If an ID is provided, the returned value is filtered with the group id
        :param idGroup:
        :return:
        """

        table = self.v_identitiesByGroups
        if idGroup:
            self.cursor.execute("select * from {table} {where}".format(table=table, where='where id_group=?'), (idGroup,))
        else:
            self.cursor.execute("select * from {table}".format(table=table))
        return self.cursor.fetchall()


    ###########
    # SETTERS #
    ###########

    def updateProject(self, ID:int, projectName:str, projectDescription:str):

        try:
            # With statement, auto commits the commands
            with self.con as con:
                con.execute("UPDATE {table} SET name=?, description=? WHERE id=?".format(table=self.t_projects)
                            ,(str(projectName), str(projectDescription), int(ID)))

        except sqlite3.Error as err:
            print("An error occurred while trying to update the database objects: "+str(err))
            return False

        return True

    def setActiveProject(self, ID:int):

        try:
            # With statement, auto commits the commands
            with self.con as con:
                con.execute("UPDATE {table} SET is_active=0 WHERE is_active=1".format(table=self.t_projects))
                con.execute("UPDATE {table} SET is_active=1 WHERE id=?".format(table=self.t_projects), (int(ID),))
        except sqlite3.Error as err:
            print("An error occurred while trying to set the project as 'active' into the database: "+str(err))
            return False

        return True

    def createNewProject(self, projectName = '', projectDescription = ''):

        table = self.t_projects
        initialMatrix = Matrix()
        projectData = initialMatrix.getRawJsonData()
        projectName = str(projectName)
        projectDescription = str(projectDescription)

        if projectName == '':
            self.errorMessage = "Project name can't be an empty string"
            return False

        try:
            self.cursor.execute("select id from {table} where name = ?".format(table=table), (projectName,))
            self.con.commit()
            if len(self.cursor.fetchall()) != 0:
                self.errorMessage = "A project with this name already exists in the database"
                return False

        except sqlite3.IntegrityError:
            print('An error occurred while adding the object to the network objects table. '
                  'You may tried to add an object that already exists')

        try:
            self.cursor.execute(
                "insert into {table} (name, description, json_project_data ) values (?, ?, ?)".format(
                    table=table), (projectName, projectDescription, projectData))
            self.con.commit()
        except sqlite3.IntegrityError:
            print('An error occurred while adding the object to the network objects table. '
                  'You may tried to add an object that already exists')

        return 1

    def updateNetObject(self, netobject=None):

        if not (isinstance(netobject, NetworkRange) or isinstance(netobject, NetworkSubnet) or isinstance(netobject, NetworkHost)):
            self.errorMessage = 'DbManager: No valid object passed as argument'
            print(self.errorMessage)
            print(netobject)
            print(str(type(NetworkHost)))
            print(str(type(netobject)))
            return False

        try:
            # With statement, auto commits the commands
            with self.con as con:
                con.execute("UPDATE {table} SET name=?, description=?, value=? WHERE id=?".format(table=self.t_network_objects)
                            ,(str(netobject.getName()), str(netobject.getDescription()), str(netobject.getValue()), str(netobject.dbID)))

        except sqlite3.Error as err:
            self.errorMessage = "An error occurred while trying to update the database objects: "+str(err)
            print(self.errorMessage)
            return False

        return True


    def addNetworkObject(self, netobject=None):
        """
        This method adds a network object to the database.
        It returns False, or the ID of the new object into the database
        :param netobject
        :return:
        """

        table = self.t_network_objects



        print('debug: starttttttttttttttt')
        # Check the type of the argument parameter.
        if not (isinstance(netobject, NetworkHost) or isinstance(netobject, NetworkRange) or isinstance(netobject, NetworkSubnet)):
            self.errorMessage = 'Argement type shoud be NetworkHost, NetworkSubnet, or NetworkRange.\nGiven argument is from type '+str(type(netobject))
            print('the host type shoéld be : ')
            print(type(NetworkHost()))
            return False

        try:
            print('debug : try.....')
            self.cursor.execute( "insert into {table} (name, description, ip_version, type, value, projectId) values (?, ?, ?, ?, ?, ?)".format(table=table), (netobject.name[:50], netobject.description[:255], netobject.ipVersion, netobject.type, netobject.getValue(), netobject.getProjectId()))
            self.con.commit()
            self.cursor.execute("select id from {table} where name = '{name}'".format(table=table, name=str(netobject.name[:50])))
            id = self.cursor.fetchall()
            return id[0][0]

        except sqlite3.IntegrityError:
            print('An error occurred while adding the object to the network objects table. '
                  'You may tried to add an object that already exists')
            return False


    def deleteNetObject(self, id:int):
        """
        Deletes the network object in table where id=id,
        and also deletes the net objects where projectId=id
        :param id:
        :return:
        """

        table = self.t_network_objects

        try:
            # With statement, auto commits the commands
            with self.con as con:
                print('Start deleting network object')
                con.execute("delete from {table} where id=?".format(table=table), (id,))
        except sqlite3.Error as err:
            print("An error occurred while trying to delete the database objects: "+str(err))
            return False

        return True

    def addIdentity(self, identityobject:Identity):
        """
        Adds an identity to the database
        :param idobject:
        :return:
        """

        table = self.t_identities
        try:
            self.cursor.execute(
                "insert into {table} (name, description) values (?, ?)".format(
                    table=table), (identityobject.getName(), identityobject.getDescription()))
            self.con.commit()
        except sqlite3.IntegrityError:
            print('An error occurred while adding the object to the network objects table. '
                  'You may tried to add an object that already exists')
            return False

    def addIdentityGroup(self, identityobject:IdentityGroup):
        """
        Adds an identity group to the database
        :param idobject:
        :return:
        """

        table = self.t_identities_groups
        try:
            self.cursor.execute(
                "insert into {table} (name, description) values (?, ?)".format(
                    table=table), (identityobject.getName(), identityobject.getDescription()))
            self.con.commit()
        except sqlite3.IntegrityError:
            print('An error occurred while adding the object to the network objects table. '
                  'You may tried to add an object that already exists')
            return False
        pass

    def checkUnsavedChanges(self):
        return filecmp.cmp(self.dbFilePath, self.dbTmpFilePath)




