import json
from Classes.NetObjects import *
from Classes.SimpleObjects import *

class Matrix:
    """
    This class provides the main matrix management functions and values
    """

    def __init__(self,xsize=3, ysize=3):
        self.xInitSize = int(xsize)
        self.yInitSize = int(ysize)

        # Id lists
        self.sourcesList = []       # A list of all used network objects ID as Source
        self.destinationsList = []  # A list of all used network objects ID as Destinations
        self.matrix = []            # The main matrix (list of list of list)
                                    #   that contains the list of services/applicaitions id used

        # Dictionaries that contains list of services, networks, etc.
        # The Identifier is an int that must be present
        self.dct_usedObjects = {}     # A dictionary that contains mathes between the ID in sourcseList or destinationList and the real obj

        # Error message
        self.errorMessage = ''

        # Init. matrix id values
        val = 0
        for a in range(0, self.yInitSize):
            row = []
            for b in range(0, self.xInitSize):
                val += 1
                row.append([])
                # Also init. some test values to check the values dump after moving columns...
            self.matrix.append(row)

        # Init xtitle list
        for a in range(0, self.xInitSize):
            self.destinationsList.append(-1)
        for a in range(0, self.yInitSize):
            self.sourcesList.append(-1)

    def dump(self):
        """
        Raw dumps refereces id from the matrix as a matrix format in cli
        followed by the sources and destination title bars (id)
        :return:
        """
        for a in range(0, self.getysize()):
            print(self.matrix[a])
        print("sources: "+str(self.getSourcesList()))
        print("destinations: "+str(self.getDestinationsList()))

    def getRawJsonData(self):

        usedObjects = {}
        supportedObjectsTypes = ['NetworkRange', 'NetworkHost', 'NetworkSubnet', 'Service', 'Application']

        for key, val in self.dct_usedObjects.items():
            objectType = str(type(val)).split('.')[-1].split('\'')[0]
            if objectType not in supportedObjectsTypes:
                self.errorMessage = "Object type '"+objectType+"' is not supported."
                return False
            else:
                usedObjects[key] = {
                    "type": objectType,
                    "dbId": val.getDbId()
                }

        jsonData = {
            "destinationsList": self.destinationsList,
            "sourcesList": self.sourcesList,
            "matrix": self.matrix,
            "dct_usedObjects": usedObjects
        }

        return json.dumps(jsonData)


    def moveColumn(self, srcPosition:int, dstPosition:int):
        """
        This method moves a column from source to destination index value
        :param srcPosition:
        :param dstPosition:
        :return:
        """
        srcPosition = int(srcPosition)
        dstPosition = int(dstPosition)

        print(self.getxsize())
        print(srcPosition)
        print(dstPosition)

        # Check goodness of arguments
        if not 0 <= srcPosition < self.getxsize() or not 0 <= dstPosition <= self.getxsize() + 1 or dstPosition == srcPosition \
            or srcPosition - dstPosition == -1:
            self.errorMessage = 'Error in moveColumn parameters. Please verify the provided arguments.'
            return False

        # Move the column
        for y in range(0, self.getysize()):
            self.matrix[y].insert(dstPosition, self.matrix[y][srcPosition])
            if srcPosition < dstPosition:
                del self.matrix[y][srcPosition]
            else:
                del self.matrix[y][srcPosition + 1]

        # Move the x title bar oder
        self.destinationsList.insert(dstPosition, self.destinationsList[srcPosition])
        if srcPosition < dstPosition:
            del self.destinationsList[srcPosition]
        else:
            del self.destinationsList[srcPosition + 1]

        return True

    def moveRow(self, srcPosition:int, dstPosition:int):
        """
        This method moves a row from source to destination index value
        :param srcPosition:
        :param dstPosition:
        :return:
        """

        # Check goodness of arguments
        if not 0 <= srcPosition < self.getysize() or not 0 <= dstPosition <= self.getysize() or dstPosition == srcPosition \
            or srcPosition - dstPosition == -1:
            self.errorMessage = 'Error in moveRow parameters. Please verify the provided arguments.'
            return False

        # Move columns from source to destination
        if dstPosition == self.getxsize() + 1:
            self.matrix.append(self.matrix[srcPosition])
            self.destinationsList.append(self.destinationsList[srcPosition])
            del self.matrix[srcPosition]
            del self.destinationsList[srcPosition]
        else:
            self.matrix.insert(dstPosition, self.matrix[srcPosition])
            self.destinationsList.insert(dstPosition, self.destinationsList[srcPosition])
            if srcPosition < dstPosition:
                del self.matrix[srcPosition]
                del self.destinationsList[srcPosition]
            else:
                del self.matrix[srcPosition + 1]
                del self.destinationsList[srcPosition + 1]

            return True

    def findUnusedIndexValues(self, newTableSize=-1):
        """
        Before creating a new line or column, we must check if there are some unused Ref ID in the matrix.
        This method finds the unused references id in the matrix (maybe after deleted row or column),
        and returns a list of unused ids.
        Even if ids can be big numbers, we suppose that we only need to check values comprised in the matrix sise,
        -> from 1 to xsize * ysize
        :return:
        """

        # Full value list initialisation
        tableSize = int(newTableSize) if int(newTableSize) > 0 else self.getysize() * self.getxsize()

        idList = []
        for a in range(0, (tableSize)):
            idList.append(a+1)

        for y in range(0, self.getysize()):
            for x in range(0, self.getxsize()):
                if self.matrix[y][x] in idList:
                    idList.remove(self.matrix[y][x])

        return idList

    def deleteRow(self, index):
        pass
        #if self.getysize() <= 1:
        #    return False
        #else:
        #    for a in range(0, self.getxsize()):
        #        print(self.matrix[index][a])

    def deleteColumn(self):
        pass

    def addColumn(self, xposition=-1):
        """
        This method adds a column to the matrix. If no argument is given, the row is append,
        else, it will be inserted at argument position
        :param xposition:
        :return:
        """

        unusedIds = self.findUnusedIndexValues(self.getxsize() * (self.getysize() +1))
        xposition = int(xposition)
        newColumn = []

        # Init column var
        while len(newColumn) < self.getysize():
            newColumn.append(unusedIds.pop(0))
        # Check for available ID for title bar.
        titleId = 0
        for a in range(1, len(self.sourcesList) + 2):
            if a not in self.sourcesList:
                titleId = a

        # Create the column and new xtitle entry
        if xposition < 0 or xposition == self.getxsize():
            self.sourcesList.append(titleId)
            for a in range(0, self.getysize()):
                # Add id into matrix cell
                self.matrix[a].append(newColumn.pop(0))
        elif 0 <= xposition < self.getxsize():
            self.sourcesList.insert(xposition, titleId)
            for a in range(0, self.getysize()):
                # Add id into matrix cell
                self.matrix[a].insert(xposition, newColumn.pop(0))


        else:
            return False

    def addRow(self, yposition=-1):
        """
        This method adds a row to the matrix. If no argument is given, the row is append,
        else, it will be inserted at argument position
        :param yposition:
        :return:
        """
        unusedIds = self.findUnusedIndexValues(self.getxsize() * (self.getysize() +1))
        yposition = int(yposition)

        # Check for available ID for title bar.
        titleId = 0
        for a in range(1, len(self.sourcesList) + 2):
            if a not in self.sourcesList:
                titleId = a

        # Init the new Row and title bar
        newRow = []
        while len(newRow) < self.getxsize():
            # add id values to the tmp row
            newRow.append(unusedIds.pop(0))

        # Add the new row and ytitle
        if yposition < 0 or yposition == self.getysize():
            self.matrix.append(newRow)
            self.destinationsList.append(titleId)
        elif 0 <= yposition < self.getysize():
            self.matrix.insert(yposition, newRow)
            self.destinationsList.insert(yposition, titleId)
        else:
            return False

        del newRow
        return True

    def getxsize(self):
        return len(self.matrix[0])

    def getysize(self):
        return len(self.matrix)

    def getSourcesList(self):
        return self.sourcesList

    def getDestinationsList(self):
        return self.destinationsList

    def getFreeKey(self):
        """
        Returns a free Key ID which is not used in dct_usedNodes
        :return:
        """
        if len(self.dct_usedObjects) == 0:
            return 1
        else:
            for a in range(1, len(self.dct_usedObjects) + 2):
                if a not in self.dct_usedObjects:
                    return a
        return -1

    def addFilter(self, x:int, y:int, snaObject, index=-1):
        """
        This function inserts the desired application, or service object id to the matrix
        The index argument can be used to manage the object ordering into the final list.
        :param x:
        :param y:
        :param snaObject:
        :param index:
        :return:
        """
        print('debug : ___ADDFILTER___')
        if not self.isSnAObjectType(snaObject) or not self.positionIsInMatrix(int(x),int(y)):
            self.errorMessage = "Index position is not valid or SnAObject is from wrong type. Provided object is from type "+str(type(snaObject))
            print(self.errorMessage)
            return False
        else:
            ID = -1
            # Check if the object is already present in the dictionary
            for key, val in self.dct_usedObjects.items():
                print(key)
                print(str(type(val)))
                print(str(type(Service())))
                print('debugggg - val.getDbId = '+str(val.getDbId()))
                print('debugggg - snaObject.getDbId = '+str(snaObject.getDbId()))
                if val.getDbId() == snaObject.getDbId():
                    ID = key

            # If the object wasn't present into the dictionary, Get a free ID value and add it to dictionary
            if ID == -1:
                print("OBJECT WAS NOT IN THE DCT")
                ID = self.getFreeKey()
                self.dct_usedObjects[ID] = snaObject
            else:
                print("OBJECT WAS ALREADY IN THE DCT")


            # Checks if the ID is still into the list
            if ID in self.matrix[y][x]:
                self.errorMessage = "The ID you want to add to the list is already there."
                return False

            # Adds the object ID tho the matrix
            if 0 <= index < len(self.matrix[y][x]):
                self.matrix[y][x].insert(index, ID)
            else:
                self.matrix[y][x].append(ID)



    def setSource(self, position:int, netObject=None):
        """
        This function sets the value (ID) of the network object to the sources list at the specified position.
        If the object isn't yet used, it will be added to the dct_usedNodes.
        If the object replaces an old object, an the old object isn't used anymore, it will be deleted from the dct_usedNodes
        :param position:
        :param netObject:
        :return:
        """
        # Check the arguments
        if not 0 <= position < self.getysize():
            self.errorMessage = "- setSource : The position passed as argument do not exists into the matrix."
            return False
        if not self.isNetworkObjectType(netObject):
            self.errorMessage = "- setSource : wrong object type passed as argument. Passed argument is from type "+str(type(netObject))
            return False

        # If a value was present before the set, and will not used anymore by destinations, we remove it from dictionnary
        if self.sourcesList[position] != -1:
            if not self.sourcesList[position] in self.destinationsList:
                del self.dct_usedObjects[self.sourcesList[position]]

        # Check if the ID is already in use in the soruces list (should not!)
        # If not it checks if it is used in destinations list, and get the ID from there
        ID = -1
        for key, item in self.dct_usedObjects.items():
            if self.isNetworkObjectType(item):
                if int(item.getDbId()) == int(netObject.getDbId()):
                    if key in self.sourcesList:
                        self.errorMessage = "- setSource : The selected object is already in the destinations list"
                        return False
                    elif key in self.destinationsList:
                        ID = key

        # Add the Key / item to the dict if it is not present
        if ID == -1:
            ID = self.getFreeKey()
            self.dct_usedObjects[ID] = netObject

        # inserts the object ID to the sources list
        self.sourcesList[position] = ID

    def setDesetination(self, position:int, netObject=None):
        """
        This function sets the value (ID) of the network object to the destination list at the specified position.
        If the object isn't yet used, it will be added to the dct_usedNodes.
        If the object replaces an old object, an the old object isn't used anymore, it will be deleted from the dct_usedNodes
        :param position:
        :param netObject:
        :return:
        """
        if not (0 <= position < self.getxsize()):
            self.errorMessage = "- setSource : The position passed as argument do not exists into the matrix."
            return False
        if not self.isNetworkObjectType(netObject):
            self.errorMessage = "- setSource : wrong object type passed as argument. Passed argument is from type "+str(type(netObject))
            return False

        # Check if the ID is already in use in the destinations list (should not!)
        # If not it checks if it is used in sources list, and get the ID from there
        ID = -1
        for key, item in self.dct_usedObjects.items():
            if self.isNetworkObjectType(item):
                if int(item.getDbId()) == int(netObject.getDbId()):
                    if key in self.destinationsList:
                        self.errorMessage = "- setSource : The selected object is already in the destinations list"
                        return False
                    elif key in self.sourcesList:
                        ID = key

        # Add the Key / item to the dict if it is not present
        if ID == -1:
            ID = self.getFreeKey()
            self.dct_usedObjects[ID] = netObject

        # and inserts the object ID to the sources list
        if ID != -1:
            self.destinationsList[position] = ID
            return True

    def removeSource(self, position:int):

        # Check if position is in sources list
        if not self.isInSourcesList(int(position)):
            return False

        # Reset the source ID from sourcesList
        uniqId = self.sourcesList[position]
        self.sourcesList[position] = -1

        # Check if the value is still used in the destinationsList
        if not uniqId in self.destinationsList:
            # Remove the uniqId from dictionary
            del self.dct_usedObjects[uniqId]
        return True

    def removeDestination(self, position:int):

        # Check if position is in sources list
        if not self.isInDestinationsList(int(position)):
            return False

        # Get the uniqId, and reset the source ID from sourcesList
        uniqId = self.destinationsList[position]
        self.destinationsList[position] = -1

        # Check if the value is still used in the destinationsList
        if not uniqId in self.sourcesList:
            # Remove the uniqId from dictionary
            del self.dct_usedObjects[uniqId]
        return True

    def removeFilter(self, x:int, y:int, object):
        """
        This function remove the ID (dictionary ID) related to the object from the matrix int the
        cell located at x;y coordinates. If the ID is not used anymore into the matrix, it is
        removed from the dictionary.
        :param x:
        :param y:
        :param object:
        :return:
        """
        # Check if the object is in the dictionary, and if the position exists. If not exits.
        if not self.isInDictionary(object):
            self.errorMessage = "The object is not into the dictionary"
            return False
        elif not self.positionIsInMatrix(x,y):
            self.errorMessage = "The specified position is not into the matrix"
            return False

        ID = self.isInDictionary(object)

        # Remove the ID from the list, and verify if it's still used into the matrix
        # If it is not usefull anymore, remove the entry from the dictionary
        if ID in self.matrix[int(y)][int(x)]:
            self.matrix[int(y)][int(x)].remove(ID)
            for y2 in range(self.getysize()):
                for x2 in range(self.getxsize()):
                    if ID in self.matrix[y2][x2]:
                        return True
            # If the ID is not anymore present into the matrix, remove the corresponding entry from dictionary
            del self.dct_usedObjects[ID]
        else:
            self.errorMessage = "Object not present at the desired position into the matrix"
            return False

    def isInDictionary(self, object):
        """
        Returns the key value of the object if it's into the used objects dictionary, or -1 if it's not.
        :param object:
        :return:
        """
        for key, val in self.dct_usedObjects.items():
            if type(val) == type(object):
                if val.getDbId() == object.getDbId():
                    return key
        return -1


    def isNetworkObjectType(self, netObject):
        """
        Checks the type of netObject
        :param netObject:
        :return:
        """
        if (type(netObject) == type(NetworkHost()) or type(netObject) == type(NetworkSubnet()) or type(netObject) == type(NetworkRange())):
            return True
        else:
            return False

    def isSnAObjectType(self, snaObject):
        """
        Check the type of snaObject.
        :param netObject:
        :return:
        """
        # Groups will be implemented later
        if isinstance(snaObject, Service) or isinstance(snaObject, Application):
            return True
        else:
            return False

    def positionIsInMatrix(self, x:int, y:int):
        if 0 <= x <= self.getxsize() and 0 <= y <= self.getysize():
            return True
        return False

    def isInSourcesList(self, position:int):
        if 0 <= position < len(self.sourcesList):
            return True
        return False

    def isInDestinationsList(self, position:int):
        if 0 <= position < len(self.destinationsList):
            return True
        return False
