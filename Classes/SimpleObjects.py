
class BaseObject:

    def __init__(self, name='', description=''):
        self.name = str(name[:255])
        self.description = str(description[:255])
        self.dbId = -1

    def setName(self, name):
        self.name = (str(name)[:255])

    def setDescription(self, description):
        self.description = str(description)[:255]

    def getName(self):
        return self.name

    def getDescription(self):
        return self.description

    def getDbId(self):
        return self.dbId


class Identity(BaseObject):
    pass


class IdentityGroup(BaseObject):

    def __init__(self, name='', description=''):
        super().__init__()
        self.name = str(name[:255])
        self.description = str(description[:255])


class Service(BaseObject):

    def __init__(self, name = '', description = '', protocol = 0, port = 0, id:int=-1, custom=0):
        """
        Initialises the service object
        :param name:
        :param description:
        :param protocol:
        :param port:
        :param id:
        :param custom:
        """
        super(Service, self).__init__()
        self.name = name[:50]
        self.description = description[:255]
        self.protocol = int(protocol)
        self.port = int(port)
        if custom != 1:
            custom = 0
        self.dbId = id

    def getProto(self):
        return self.protocol

    def getPort(self):
        return self.port

    def getDbId(self):
        return self.dbId

    def setProto(self, proto:int):
        self.proto = int(proto)

    def setPort(self, port:int):
        self.port = int(port)

    def load(self, service:tuple):
        """
        Loads item values from a tuple from DbManager.getServices(ID)
        :param service:
        :return:
        """
        service=service[0]
        if len(service) != 6:
            return False
        self.dbId = service[0]
        self.name = str(service[1])
        self.description = str(service[2])
        self.protocol = int(service[3])
        self.port = int(service[4])


class ServiceGroup(BaseObject):

    def load(self, servicegroup:tuple):
        """
        Loads item values from a tuple from DbManager.getServiceGroups(ID)
        :param servicegroup:
        :return:
        """
        servicegroup=servicegroup[0]
        if len(servicegroup) != 4:
            return False
        self.name = str(servicegroup[1])
        self.description = str(servicegroup[2])


class Application(BaseObject):

    def __init__(self, name='', description='', id:int = -1, custom=0):
        super(Application, self).__init__()
        """
        Initialises the application object
        :param app:
        :return:
        """
        if custom != 1:
            custom = 0
        self.dbId = id
        self.name = str(name)
        self.description = str(description)


class ApplicationGroup(BaseObject):

    def load(self, appGroup:tuple):
        """
        Loads item values from a tuple from DbManager.getApplicationsGroups(ID)
        :param appGroup:
        :return:
        """
        appGroup=appGroup[0]
        if len(appGroup) != 4:
            return False
        self.name = str(appGroup[1])
        self.description = str(appGroup[2])
