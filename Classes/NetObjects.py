
class NetParentObject:
    """
    This Classes is a parent Classes for basic features heritage
    """
    def __init__(self,projectId=-1):

        self.errorMessage = ''
        self.rawErrorMessage = ''
        self.ipVersion = 0
        self.name = ''
        self.description = ''
        self.type = 0
        self.dbID = -1
        self.projectId = int(projectId)

    def isIPv4(self, s):
        try:
            return str(int(s)) == s and 0 <= int(s) <= 255
        except:
            return False

    def isIPv6(self, s):
        if s == '':
            return True
        if len(s) > 4:
            return False
        try:
            return int(s, 16) >= 0 and s[0] != ''
        except:
            return False

    def getIPVersion(self, IP):

        # Check the full ipv4 string format for first and last IP
            if IP.count(".") == 3 and all(self.isIPv4(i) for i in IP.split(".")) \
                    and IP.split('.')[0] != '0' \
                    and IP.split('.')[0] != '':
                return 4

            # Check the full ipv6 string format
            if IP.count(":") <= 7 and all(self.isIPv6(i) for i in IP.split(":")) \
                    and IP.split(':')[0] != '0' \
                    and IP.split(':')[0] != '' \
                    and IP.split(':').count('') < 2:
                return 6

            return False

    def incorrectFormat(self, s):
        self.errorMessage = 'Incorrect '+s+' string format'
        return False


    ###########
    # Getters #
    ###########


    def getName(self):
        return self.name

    def getProjectId(self):
        return self.projectId

    def getDbId(self):
        return self.dbID

    def getDescription(self):
        return self.description

    def getIpVersion(self):
        return self.ipVersion

    def getErrorMessage(self):
        return self.errorMessage

    def getRawErrorMessage(self):
        return self.rawErrorMessage




    ###########
    # Setters #
    ###########

    def setName(self, name):
        self.name = str(name)[:50]

    def setDescription(self, description):
        self.description = str(description)[:255]

    def setProjectId(self, projectId):
        self.projectId = int(projectId)



class NetworkHost(NetParentObject):

    # This Classes describes network host nodes objects

    def __init__(self, name='', description='', value='', projectId=-1, dbId:int=-1):
        super().__init__()
        self.IP = ''
        self.ipVersion = 0
        self.type = 1
        self.dbID = int(dbId)
        self.projectId = int(projectId)

        print('debug::::::::::::: before')
        print(name+' '+description+' '+value+' '+str(projectId))
        print(self.validateIPAddress(value))
        # If the given value is correct, set the IP, name and description
        if value != '' and self.setIP(value):
            self.setName(name)
            self.setDescription(description)
            self.setIP(value)
        else:
            print('debug::::::::::::. else')
            print(self.setIP(value))


    def setIP(self, ip):
        """
        Sets the IP address and related ip version if the format is correct
        :param ip:
        :return:
        """
        self.ipVersion = self.validateIPAddress(ip)
        self.IP = ip if self.ipVersion != 0 else ''
        return self.ipVersion

    def getValue(self):
        return self.IP

    def getIP(self):
        return self.IP

    def validateIPAddress(self, IP):
        """
        :type IP: str
        :rtype: bool
        This function checks the format of an IP host address
        The self.ipVersion variable is set to 4 or 6 if the string is correctly formatted
        """

        # Check the full ipv4 string format
        if IP.count(".") == 3 and all(self.isIPv4(i) for i in IP.split(".")) \
                and IP.split('.')[0] != '0' \
                and IP.split('.')[0] != '':
            self.ipVersion = 4
            return 4

        # Check the full ipv6 string format
        if IP.count(":") <= 7 and all(self.isIPv6(i) for i in IP.split(":")) \
                and IP.split(':')[0] != '0' \
                and IP.split(':')[0] != '' \
                and IP.split(':').count('') < 2:
            self.ipVersion = 6
            return 4

        self.incorrectFormat('host')
        return 0


class NetworkSubnet(NetParentObject):
    """
    This Classes describes IPV4 or IPV6 Subnets objects
    """

    def __init__(self, name = '', description = '', value = '', projectId=-1, dbId:int=-1):
        super().__init__()
        self.subnetAddress = ''
        self.ipVersion = 0
        self.type = 2
        self.dbID = int(dbId)
        self.projectId = int(projectId)

        # If the given value is correct, set the IP, name and description
        if value != '':
            if self.setSubnetAddress(value):
                self.setName(name)
                self.setDescription(description)
        else:
            self.setName(name)
            self.setDescription(description)


    def validateSubnetIP(self, string:str):
        """
        This function checks the format of the string in argument.
        This string must be to ipv4 or ipv6 format (CIDR)
        Exemples :
            ipv4 :  192.168.2.0/24
            ipv6 :  a:b:c:d:e:f:1:2:3/64
                    2001:abde:ff32::/100

        The self.ipVersion variable is set to 4 or 6 if the string is correctly formatted

        :param string:
        :return: boolean
        """
        print('debug - validateSubnetIp() -> pos1')

        # Check
        subnetstring = string.split('/')
        if len(subnetstring) != 2:
            print('debug - validateSubnetIp() -> return 0')
            return 0
        self.IP = subnetstring[0]
        self.MASK = subnetstring[1]

        # Check the full ipv4 subnet string format
        if self.IP.count(".") == 3 and all(self.isIPv4(i) for i in self.IP.split(".")) and self.IP.split('.')[0] != '0' and self.IP.split('.')[0] != '':
            # Check ipv4 subnet mask
            if 0 < int(subnetstring[1]) < 32:
                self.ipVersion = 4
                self.value = string
                return 4

        # Check the full ipv6 subnet string format
        if self.IP.count(":") <= 7 and all(self.isIPv6(i) for i in self.IP.split(":")) and self.IP.split(':')[0] != '0' and self.IP.split(':')[0] != '':
            # The max empty values in IPV6 is 2
            if self.IP.split(':').count('') > 2:
                return self.incorrectFormat('subnet')
            # If there are empty octets, check if the position is authorized in ipv6 network notation
            if self.IP.split(':').count('') <= 2:
                if self.IP.split(':').count('') == 0 and len(self.IP.split(':')) != 8:
                    return self.incorrectFormat('subnet')
                if self.IP.split(':').count('') == 2 and (not (self.IP.split(':')[-1] != '' or not self.IP.split(':')[-2] != '')):
                    return self.incorrectFormat('subnet')
            # If IPV6 format address is correct, check the ipv6 subnet mask
            if 0 < int(subnetstring[1]) < 126:
                self.ipVersion = 6
                return 6

        return self.incorrectFormat('subnet')


    ###########
    # Getters #
    ###########

    def getValue(self):
        return self.subnetAddress

    def getSubnetAddress(self):
        return self.subnetAddress


    ###########
    # Setters #
    ###########


    def setSubnetAddress(self, address):
        """
        Sets the IP address and related ip version if the format is correct
        :param ip:
        :return:
        """
        # Checks the valide subnet string, and returns the ip version, or 0 if none is detected
        self.ipVersion = self.validateSubnetIP(address)
        if self.ipVersion:
            self.subnetAddress = address
            return True
        else:
            return False


class NetworkRange(NetParentObject):
    """
    This Classes describes network ip ranges oajects
    """

    def __init__(self, name:str = '', description:str = '', value:str = '', projectId=-1, dbId:int=-1):
        super(NetworkRange, self).__init__()
        self.firstIP = ''
        self.lastIP = ''
        self.range = ''
        self.type = 3
        self.name = str(name)
        self.dbID = int(dbId)
        self.description = str(description)
        self.validateIPRange(str(value))
        self.projectId = int(projectId)

    def isInRange(self, ip:str):
        """
        Checks if the passed ip is in the range
        :param ip:
        :return:
        """

        if self.ipVersion != self.getIPVersion(ip):
            print('--- is in range ---')
            print (self.ipVersion)
            print (self.getIPVersion(ip))
            self.errorMessage = 'IP string format do not match the IP version of the object'
            return False

        # Define split character for the ip version
        splitchar = '.' if self.ipVersion == 4 else 6
        # Define tmp vars
        ip1 = self.firstIP.split(splitchar)
        ip2 = ip.split(splitchar)
        ip3 = self.lastIP.split(splitchar)

        # Check if ip2 is in the range
        for octet in range(0,len(ip1)):
            if ip1 > ip2 or ip2 > ip3:
                return False
        return True

    def validateIPRange(self, string):
        """
        :type IP: str
        :rtype: str or bool

        This function validates the IP range formatting
        IP string must be formatted with 'FirstIPAddress/LastIncludedAddress',
        like this '10.10.1.20/10.10.1.30' or '10.1.1.1/10.1.50.9'
        """

        IPs = string.split('/')
        if len(IPs) != 2:
            return False

        # Check if the 2 ip address are from same version and correct format
        print('--- validate ip range ---')
        print(self.ipVersion)
        self.ipVersion = self.getIPVersion(IPs[0])
        print(self.ipVersion)
        if not ((self.getIPVersion(IPs[0]) == self.getIPVersion(IPs[1]) and self.ipVersion != False)):
            return self.incorrectFormat('IP range')

        # Init vars for tho used ipVersion
        if int(self.ipVersion) == 4:
            splitChar = '.'
            baseInt = 10
        else:
            splitChar = ':'
            baseInt = 16

        # Set IPs
        IP1 = IPs[0].split(splitChar)
        IP2 = IPs[1].split(splitChar)

        # Check if IP1 is lower than IP2
        for i in range(0,len(IP1)):
            if int(IP1[i], base=baseInt) > int(IP2[i], base=baseInt):
                self.rawErrorMessage = 'First IP must have lower value as the second ip'
                return self.incorrectFormat('IP range')

        self.firstIP = IPs[0]
        self.lastIP = IPs[1]
        self.range = string

        return self.ipVersion


    ###########
    # Getters #
    ###########

    def getValue(self):
        return self.range

    def getRange(self):
        return self.range

    def getFirstIP(self):
        return self.firstIP

    def getLastIP(self):
        return self.lastIP

    def getDbId(self):
        return self.dbID


    ###########
    # SETTERS #
    ###########

    def setRange(self, range):
        # Checks the valide subnet string, and returns the ip version, or 0 if none is detected
        print('--- set range ---')
        print(self.ipVersion)
        self.ipVersion = self.validateIPRange(range)
        print(self.ipVersion)
        if self.ipVersion:
            self.range = range
            return True
        else:
            return False



