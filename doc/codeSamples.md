#Sources and destinations configuration
## Set the source and/or destination
1. Load the object from database
2. Insert de network object to the source and/or destination list

~~~python
from Classes.DbManager import *
from Classes.Matrix import *

# if needed, connect to the database
dbManager = DbManager()
dbManager.connect('/home/cinux/test.db')
# define the matrix object if not existing ...
matrix = Matrix(4, 5)

# Create a network object and add it to source and /or destination
# The object can be loaded frow database as following (id=3)
host1 = dbManager.loadNetworkNode(3)
if not matrix.setSource(0, host1):
    print(matrix.errorMessage)
if not matrix.setDesetination(5, host1):
    print(matrix.errorMessage)
~~~

## Add a filter value to the matrix
1. Loads the service/application from database
2. Add the filter to the matrix

~~~python
from Classes.DbManager import *
from Classes.Matrix import *

# if needed, connect to the database
dbManager = DbManager()
dbManager.connect('/home/cinux/test.db')
# define the matrix object if not existing ...
matrix = Matrix(4, 5)
service1 = dbManager.loadService(3)
app1 = dbManager.loadApplication(1)

if not service1:
    print('Erreur de chargement de service1')
else:
    if not matrix.addFilter(0, 0, service1):
        print('Error...')
~~~

